<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Invoice;
use App\Item;
use Validator;
use App\Http\Resources\Invoice as InvoiceResource;
use App\Http\Resources\Item as ItemResource;
use DB;
class InvoiceController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $invoices = Invoice::all();

        return $this->sendResponse(InvoiceResource::collection($invoices), 'Invoices retrieved successfully.');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'number' => 'required|unique:invoices',
            'status' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $invoice = Invoice::create($input);

        return $this->saveItems($invoice, $request->items);
    }

    private function saveItems( $invoice, $items ){

      $amount=0;
      foreach ($items as $item) {

        $validator = Validator::make($item, [
            'number' => 'required',
            'description' => 'required',
            'quantity' => 'required',
            'price' => 'required'
        ]);
        if($validator->fails()){
          $invoice->delete();

          return $this->sendError('Validation Error.', $validator->errors());
        }
        $amount+= $item['price']*$item['quantity'];
        $invoice->items()->create($item);

      }
      $invoice->amount = $amount;
      $invoice->save();
      return $this->sendResponse(new InvoiceResource($invoice), 'Invoice created successfully.');
    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
   public function show($id)
   {
       $invoice = Invoice::find($id);

       if (is_null($invoice)) {
           return $this->sendError('Invoice not found.');
       }
       return $this->sendResponse(new InvoiceResource($invoice, $invoice->items), 'Invoice retrieved successfully.');
   }

   /**
     * Display list of invoices between 2 dates.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function range(Request $request)
    {
      $invoices = DB::table('invoices')
           ->whereBetween('created_at', [$request->start, $request->end])
           ->get();

      foreach ($invoices as $invoice) {
        $invoice->items =  Item::where("invoice_id", "=", $invoice->id)->get();
      }

      return $invoices;
    }

    /**
    * Display list of invoices with status true.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function paid_invoices()
    {
      $invoices = DB::table('invoices')
               ->where('status', 1)
               ->get();

      foreach ($invoices as $invoice) {
            $invoice->items =  Item::where("invoice_id", "=", $invoice->id)->get();
      }
      return $invoices;
    }

    /**
     * Display list of invoices with status true.
     *
     * @param  Boolean status
     * @return \Illuminate\Http\Response
     */
    public function subtotal(Request $request)
    {
      return DB::table('invoices')
           ->where('status', $request->status)
           ->sum('amount');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Invoice $invoice)
    {
        $invoice->delete();
        $invoice->items()->delete();

        return $this->sendResponse([], 'Invoice deleted successfully.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Invoice $invoice)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'number' => 'required',
            'status' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $invoice->number = $input['number'];
        $invoice->status = $input['status'];
        $invoice->save();

        return $this->updateItems($invoice, $request->items);
    }


    private function updateItems($invoice, $items){

      $amount=0;
      foreach ($items as $item) {

        $validator = Validator::make($item, [
            'number' => 'required',
            'description' => 'required',
            'quantity' => 'required',
            'price' => 'required'
        ]);
        if($validator->fails()){
          return $this->sendError('Validation Error.', $validator->errors());
        }

        $amount+= $item['price']*$item['quantity'];
      
        Item::where(
          [
            ['invoice_id', '=', $invoice->id],
            ['number', '=', $item["number"]]])->
            update([
                    "description" => $item["description"],
                    "quantity"=>$item["quantity"],
                    "price"=>$item["price"],
                  ]);
      }
      $invoice->amount = $amount;
      $invoice->save();
      return $this->sendResponse(new InvoiceResource($invoice), 'Invoice created successfully.');

    }

}
