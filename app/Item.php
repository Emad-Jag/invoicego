<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    //
    protected $fillable = [
        'description', 'quantity', 'price', 'number'
    ];
    public $timestamps = false;

    public function invoice(){
      return $this->belongsTo(Invoice::class);
    }
}
