<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $fillable = [
      'number', 'status',"invoice_id"
    ];

    protected $attributes = [
       'amount' => 0,
    ];

    //
    public function items(){
      return $this->hasMany(Item::class);
    }


}
