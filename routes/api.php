<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::get('invoices/range', 'API\InvoiceController@range');
Route::get('invoices/paid', 'API\InvoiceController@paid_invoices');
Route::get('invoices/total', 'API\InvoiceController@subtotal');

Route::resource('invoices', 'API\InvoiceController');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
